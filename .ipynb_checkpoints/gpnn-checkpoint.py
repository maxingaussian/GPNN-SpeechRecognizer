import torch
import torch.nn as nn
import torch.utils.data as Data
from torch.autograd import Variable
import matplotlib.pyplot as plt
import torch.nn.functional as F
import numpy as np

class SGPALayer(nn.Module):
    def __init__(self, in_features, spectral_features, out_features, gpu_id=None, fixed_epsilon=False):
        super(SGPALayer, self).__init__()
        self.in_features = in_features
        self.spectral_features = spectral_features
        self.out_features = out_features
        self.fixed_epsilon = fixed_epsilon
        self.gpu_id = gpu_id
        self.Z_mean = nn.Parameter(torch.Tensor(2*in_features, spectral_features).cuda(self.gpu_id))
        self.Z_lgstd = nn.Parameter(torch.Tensor(2*in_features, spectral_features).cuda(self.gpu_id))
        self.p = nn.Parameter(torch.Tensor(2, spectral_features).cuda(self.gpu_id))
        # self.affine_mapping = nn.Linear(2*spectral_features, out_features)
        if(fixed_epsilon):
            self.epsilon = Variable(torch.randn(2*self.in_features, self.spectral_features).cuda(self.gpu_id))
        self.init_parameters()

    def init_parameters(self):
        nn.init.xavier_normal(self.p)
        nn.init.xavier_normal(self.Z_mean)
        nn.init.xavier_normal(self.Z_lgstd)
    
    def kl_divergence(self):
        mean2 = self.Z_mean**2.
        std2 = torch.exp(self.Z_lgstd*2)
        lgvar = 2*self.Z_lgstd
        return 0.5*torch.sum(mean2+std2-lgvar-1)

    def forward(self, input):
        Z_std = torch.exp(self.Z_lgstd)
        if(self.fixed_epsilon):
            Z1, Z2 = (self.Z_mean+self.epsilon*Z_std).chunk(2)
        else:
            epsilon = Variable(torch.randn(2*self.in_features, self.spectral_features)).cuda(self.gpu_id)
            Z1, Z2 = (self.Z_mean+epsilon*Z_std).chunk(2)
        p1, p2 = self.p.chunk(2)
        Z1, Z2 = input.mm(Z1), input.mm(Z2)
        Z1, Z2 = Z1+p1.expand_as(Z1), Z2+p2.expand_as(Z2)
        S1, S2 = torch.cos(Z1)+torch.cos(Z2), torch.sin(Z1)+torch.sin(Z2)
        spectral_input = torch.cat([S1, S2], 1)/np.sqrt(self.spectral_features)
        output = spectral_input
        # output = self.affine_mapping(spectral_input)
        return output
    def __repr__(self):
        return self.__class__.__name__ + '(' \
            + 'in_features=' + str(self.in_features) \
            + ', spectral_features=' + str(self.spectral_features) \
            + ', out_features=' + str(self.out_features) + ')'


class SGPA(nn.Module):
    def __init__(self, layer_sizes, spectral_features=500, train_sample_size=10, eval_sample_size=100, gpu_id=None):
        super(SGPA, self).__init__()
        self.layer_sizes = layer_sizes
        self.spectral_features = spectral_features
        self.eval_sample_size = eval_sample_size
        self.train_sample_size = train_sample_size
        self.gpu_id = gpu_id
        self.sgpa_layer1 = SGPALayer(layer_sizes[0], spectral_features, layer_sizes[1], self.gpu_id)
        self.dnn = self.linear_network(self.layer_sizes)

    def multilinear_network(self, layer_sizes):
        dnn = nn.Sequential()
        for l, (n_in, n_out) in enumerate(zip(layer_sizes[1:-1], layer_sizes[2:])):
            layer = nn.Linear(n_in+layer_sizes[0], n_out)
            r = 4 *(6.0 / (n_in+layer_sizes[0] + n_out))**0.5
            layer.weight.data.uniform_(-1.0 * r, r)
            layer.bias.data.fill_(0.0)
            dnn.add_module('linear'+str(l), layer)
            if(l < len(layer_sizes)-3):
                dnn.add_module('Sigmoid'+str(l), nn.Sigmoid())
        return dnn

    def kl_cost(self):
        KL = self.sgpa_layer1.kl_divergence()
        return KL

    def single_pass(self, input):
        output = self.sgpa_layer1.cuda(self.gpu_id)(input.cuda(self.gpu_id))
        for l in range(len(self.dnn)):
            if l % 2 == 0:
                output = self.dnn[l].cuda(self.gpu_id)(torch.cat([input.cuda(self.gpu_id), output], 1))
            else: 
                output = self.dnn[l](output)
        return output

    def forward(self, input):
        if(self.training):
            sample_size = self.train_sample_size
        else:
            sample_size = self.eval_sample_size
        output = Variable(torch.Tensor(sample_size, input.size(0), self.layer_sizes[-1])).cuda(self.gpu_id)
        for s in range(sample_size):
            output[s, :, :] = self.single_pass(input)
        output = torch.mean(output, 0)
        return output