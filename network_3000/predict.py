import torch
import torch.nn as nn
import torch.utils.data as Data
from torch.autograd import Variable
import matplotlib.pyplot as plt
import pandas as pd
import torch.nn.functional as F
import numpy as np
import pickle
gpu_id= 0

with open('../prior.pickle','rb') as prior_file:
    prior = pickle.load(prior_file)

def concat(input_data):
    row_num = input_data.shape[0]
    column_num = input_data.shape[1]
    X_input = np.zeros((row_num, 9*column_num))
    for i in range(row_num):
        if i == 0:
            X_input[i,:] = np.hstack((input_data[i,:],input_data[i,:],input_data[i,:],input_data[i,:],input_data[i,:],input_data[i+1,:],\
        input_data[i+2,:],input_data[i+3,:],input_data[i+4,:]))
        elif i == 1:
            X_input[i,:] = np.hstack((input_data[i-1,:],input_data[i-1,:],input_data[i-1,:],input_data[i-1,:],input_data[i,:],input_data[i+1,:],\
        input_data[i+2,:],input_data[i+3,:],input_data[i+4,:]))
        elif i == 2:
            X_input[i,:] = np.hstack((input_data[i-2,:],input_data[i-2,:],input_data[i-2,:],input_data[i-1,:],input_data[i,:],input_data[i+1,:],\
        input_data[i+2,:],input_data[i+3,:],input_data[i+4,:]))
        elif i == 3:
            X_input[i,:] = np.hstack((input_data[i-3,:],input_data[i-3,:],input_data[i-2,:],input_data[i-1,:],input_data[i,:],input_data[i+1,:],\
        input_data[i+2,:],input_data[i+3,:],input_data[i+4,:]))
        elif i == row_num-4:
            X_input[i,:] = np.hstack((input_data[i-3,:],input_data[i-3,:],input_data[i-2,:],input_data[i-1,:],input_data[i,:],input_data[i+1,:],\
        input_data[i+2,:],input_data[i+3,:],input_data[i+3,:]))
        elif i == row_num-3:
            X_input[i,:] = np.hstack((input_data[i-3,:],input_data[i-3,:],input_data[i-2,:],input_data[i-1,:],input_data[i,:],input_data[i+1,:],\
        input_data[i+2,:],input_data[i+2,:],input_data[i+2,:]))
        elif i == row_num-2:
            X_input[i,:] = np.hstack((input_data[i-3,:],input_data[i-3,:],input_data[i-2,:],input_data[i-1,:],input_data[i,:],input_data[i+1,:],\
        input_data[i+1,:],input_data[i+1,:],input_data[i+1,:]))
        elif i == row_num-1:
            X_input[i,:] = np.hstack((input_data[i-3,:],input_data[i-3,:],input_data[i-2,:],input_data[i-1,:],input_data[i,:],input_data[i,:],\
        input_data[i,:],input_data[i,:],input_data[i,:]))
        else:
            X_input[i,:] = np.hstack((input_data[i-4,:],input_data[i-3,:],input_data[i-2,:],input_data[i-1,:],input_data[i,:],input_data[i+1,:],\
        input_data[i+2,:],input_data[i+3,:],input_data[i+4,:]))
    return X_input

#define network
layer_sizes = [351, 500, 500, 500, 500, 500, 689]
dnn = nn.Sequential()
for l, (n_in, n_out) in enumerate(zip(layer_sizes[:-1], layer_sizes[1:])):
    # dnn.add_module('batch_norm'+str(l), nn.BatchNorm1d(n_in))
    dnn.add_module('linear'+str(l), nn.Linear(n_in, n_out))
    if(l < len(layer_sizes)-2):
        dnn.add_module('Sigmoid'+str(l), nn.Sigmoid())
    else: dnn.add_module('Softmax'+str(l), nn.Softmax(dim=1))

dnn.load_state_dict(torch.load('/scratch/bdda/skhu/GPNN/dnn_baseline/finetune/best_net_sgd.pkl'))
print('load network successfully')
#load test data
for scp_name in ['ind_feb89', 'ind_feb91', 'ind_oct89', 'ind_sep92']:
    TEST_DATA_PATH = '/scratch/bdda/skhu/RM/gpnn_baseline/flists/'+scp_name+'.scp'
    f3 = open(TEST_DATA_PATH, 'r')
    line3 = f3.readlines()
    write_path = scp_name+'.output'
    f = open(write_path, 'w')
    with torch.cuda.device(gpu_id):
        dnn = dnn.cuda()
        for line in line3:
            line = line.strip()
            print(line)
            line_label = line.replace('.mfc','.csv')
            line_label = line_label.replace('inputdata_txt','outputdata_label')
            file_name = line_label.split('/')[-1]
            matrix1 = pd.read_table(line,'\s+',index_col=None,header=None)
            X_input = concat(np.array(matrix1))
            X_test = torch.FloatTensor(X_input)
            test_x = Variable(X_test).cuda()
            test_output = dnn(test_x)
            output = np.array(test_output.data) + (1e-40)*np.ones(689)
            output = np.log(output)-np.log(prior)
            f.write(file_name.split('.')[0] + '  [' + '\n')
            R = output.shape[0]
            for i in range(R-1):
                f.write('  ')
                f.write(' '.join(str(v) for v in output[i,:].tolist()))
                f.write(' \n')
            f.write('  ')
            f.write(' '.join(str(v) for v in output[R-1,:].tolist())+' ]')
            f.write('\n')
    f3.close()
    f.close()
