%load_ext autoreload
%autoreload 2

import torch
import torch.nn as nn
import torch.utils.data as Data
from torch.autograd import Variable
import matplotlib.pyplot as plt
import torch.nn.functional as F
import numpy as np
from model.speech_dataloader import thread_loaddata
from model.gpnn import SGPALayer, SGPA
from model.clip_grad_abs import clip_grad_abs 
from model.learning_rate_schudule import learning_rate_schudule
import time


class Acoustic_model(object):
    def __init__(self, layer_sizes, trian_path, valid_path, gpu_id, train_num, valid_num, batch_size):
        self.layer_sizes = layer_sizes
        self.gpu_id = gpu_id
        self.train_loader = thread_loaddata(trian_path, 500)
        self.valid_loader = thread_loaddata(valid_path, 4)
        self.train_num = train_num
        self.valid_num = valid_num
        self.batch_size = batch_size
        self.loss_func = nn.CrossEntropyLoss().cuda()
        self.sgpa = None
        self.optimizer = None
    
    def pretrain(self, EPOCH):
        with torch.cuda.device(self.gpu_id):
            for epoch in range(EPOCH):
                pretrain_layer_sizes = []
                pretrain_layer_sizes.append(self.layer_sizes[0])
                for i in range(epoch+1):
                    pretrain_layer_sizes.append(self.layer_sizes[i+1])
                pretrain_layer_sizes.append(self.layer_sizes[-1])
                if epoch == 0 :
                    self.sgpa = SGPA(pretrain_layer_sizes, 250, 1, 1, gpu_id = self.gpu_id)                   
                    self.optimizer = torch.optim.SGD(self.sgpa.parameters(), lr = 0.001*800, momentum=0.5, weight_decay = 0.001/800)
                else: self.sgpa, self.optimizer = self.reload_network(pretrain_layer_sizes)
                self.sgpa = self.sgpa.cuda()
                self.sgpa.train()
                running_time = -time.time()
                train_loss = 0
                train_ll_loss = 0
                train_kl_loss = 0
                train_accuracy = 0
                for iter, (batch_x, batch_y) in enumerate(self.train_loader):
                    if batch_y.size(0) % self.batch_size != 0:
                        total_batch_num = batch_y.size(0)//self.batch_size+1
                    else: total_batch_num = batch_y.size(0)//self.batch_size
                    for batch_num in range(total_batch_num):
                        b_x = Variable(batch_x[batch_num*self.batch_size:(batch_num+1)*self.batch_size,:]).cuda()
                        b_y = Variable(batch_y[batch_num*self.batch_size:(batch_num+1)*self.batch_size]).cuda()
                        train_output = self.sgpa(b_x)
                        train_KL = self.sgpa.kl_cost()
                        loss, nll, kl = self.cal_loss(b_y, train_output, train_KL)
                        self.optimizer.zero_grad()
                        loss.backward()
                        clip_grad_abs(self.sgpa.parameters(), 0.32)
                        self.optimizer.step()
                        train_loss += loss.data[0]
                        train_ll_loss += nll.data[0]
                        train_kl_loss += kl.data[0]
                        train_y_decoded = torch.max(train_output, 1)[1].data.squeeze()
                        train_accuracy += sum(train_y_decoded == b_y.data) / self.train_num                       
                running_time += time.time()
                print('Epoch: ', epoch, '| time: %2fs' % running_time, '| train loss: %.2f' % train_loss, '| train accuracy: %.4f' % train_accuracy, '| train_ll_loss: %.2f' % train_ll_loss, '| train_kl_loss: %.2f' % train_kl_loss)
                self.sgpa.eval()
                self.valid_accuracy, valid_loss = self.valid(epoch)                
                if epoch % 1 == 0 :
                    best_state = self.sgpa.state_dict()
                    self.sgpa.load_state_dict(best_state)
                    model_name = 'model_parameters/gpnn_net_pretrained_' + str(int(epoch+1)) + '.pkl' 
                    torch.save(best_state, model_name)
    
    def train(self, EPOCH):
        self.sgpa = SGPA(self.layer_sizes, 250, 1, 1, gpu_id = self.gpu_id)
        self.optimizer = torch.optim.SGD(self.sgpa.parameters(), lr = 0.002*800, momentum=0.5, weight_decay = 0.001/800)
        dict_trained = torch.load('model_parameters/gpnn_net_pretrained_4.pkl', map_location=lambda storage, loc: storage.cuda(gpu_id))
        dict_new = self.sgpa.state_dict()
        trained_list = list(dict_trained.keys())
        new_list = list(dict_new.keys())
        for i in range(3+2*(len(self.layer_sizes)-4)):
            dict_new[ new_list[i] ] = dict_trained[ trained_list[i] ]
        self.sgpa.load_state_dict(dict_new)
        local_monitor_value = 0
        best_valid_accuracy = 0
        best_state = {}
        max_iter = 0
        count = 0
        with torch.cuda.device(self.gpu_id):
            for epoch in range(EPOCH):                            
                self.sgpa = self.sgpa.cuda()
                self.sgpa.train()
                running_time = -time.time()
                train_loss = 0
                train_ll_loss = 0
                train_kl_loss = 0
                train_accuracy = 0
                for iter, (batch_x, batch_y) in enumerate(self.train_loader):
                    if batch_y.size(0) % self.batch_size != 0:
                        total_batch_num = batch_y.size(0)//self.batch_size+1
                    else: total_batch_num = batch_y.size(0)//self.batch_size
                    for batch_num in range(total_batch_num):
                        b_x = Variable(batch_x[batch_num*self.batch_size:(batch_num+1)*self.batch_size,:]).cuda()
                        b_y = Variable(batch_y[batch_num*self.batch_size:(batch_num+1)*self.batch_size]).cuda()
                        train_output = self.sgpa(b_x)
                        train_KL = self.sgpa.kl_cost()
                        loss, nll, kl = self.cal_loss(b_y, train_output, train_KL)
                        self.optimizer.zero_grad()
                        loss.backward()
                        clip_grad_abs(self.sgpa.parameters(), 0.32)
                        self.optimizer.step()
                        train_loss += loss.data[0]
                        train_ll_loss += nll.data[0]
                        train_kl_loss += kl.data[0]
                        train_y_decoded = torch.max(train_output, 1)[1].data.squeeze()
                        train_accuracy += sum(train_y_decoded == b_y.data) / self.train_num
                learning_rate = self.optimizer.param_groups[0]['lr']
                running_time += time.time()
                print('Epoch: ', epoch, '| time: %2fs' % running_time, '| learning rate: %e' % learning_rate, '| train loss: %.2f' \
                      % train_loss, '| train accuracy: %.4f' % train_accuracy, '| train_ll_loss: %.2f' % train_ll_loss, \
                      '| train_kl_loss: %.2f' % train_kl_loss)
                self.sgpa.eval()
                valid_accuracy, valid_loss = self.valid()
                if best_valid_accuracy < valid_accuracy:
                    best_valid_loss = valid_loss
                    best_valid_accuracy = valid_accuracy
                    local_monitor_value = valid_accuracy
                    count = 0
                    self.sgpa.cpu()
                    best_state = self.sgpa.state_dict()
                    self.sgpa.load_state_dict(best_state)
                    torch.save(best_state, 'model_parameters/best_net_sgd.pkl') 
                    self.sgpa.cuda()
                else: 
                    count += 1
                self.optimizer, self.sgpa = learning_rate_schudule(self.sgpa, self.optimizer, valid_accuracy, local_monitor_value, \
                                                                   best_state, startdiff = 0.001, mode = 'max', factor = 0.5, patience = 2)
                print('Epoch: ', epoch, '| best valid loss: %.4f' % best_valid_loss, '| best valid accuracy: %.4f' % best_valid_accuracy, \
                      '| valid loss: %.4f' % valid_loss, '| valid accuracy: %.4f' % valid_accuracy)
                if count > max_iter:
                    print('No improvement for %d epochs' % max_iter)
                    break     
    
    def valid(self):
        valid_loss = 0
        valid_accuracy = 0
        for step, (batch_valid_x, batch_valid_y) in enumerate(self.valid_loader):
            b_valid_x = Variable(batch_valid_x).cuda()
            b_valid_y = Variable(batch_valid_y).cuda()
            valid_output = self.sgpa(b_valid_x)
            valid_KL = self.sgpa.kl_cost()
            loss, nll, kl = self.cal_loss(b_valid_y, valid_output, valid_KL)
            valid_loss += nll.data[0]
            valid_y_decoded = torch.max(valid_output, 1)[1].data.squeeze()
            valid_accuracy += torch.sum(valid_y_decoded == b_valid_y.data) / self.valid_num
        return valid_accuracy, valid_loss
    
    def cal_loss(self, y, output, KL):
        nll = self.loss_func(output,y)
        kl = KL/output.size(0)
        loss = nll + kl
        return loss, nll, kl
    
    def reload_network(self, layer_sizes):
        Num_layer = len(layer_sizes)
        self.sgpa = SGPA(layer_sizes, 250, 1, 1, gpu_id = self.gpu_id)
        model_name = 'model_parameters/gpnn_net_pretrained_' + str(int(Num_layer-3)) + '.pkl' 
        dict_trained = torch.load( model_name )
        dict_new = self.sgpa.state_dict()
        trained_list = list(dict_trained.keys())
        new_list = list(dict_new.keys())
        for i in range(3+2*(Num_layer-4)):
            dict_new[ new_list[i] ] = dict_trained[ trained_list[i] ]
        self.sgpa.load_state_dict(dict_new)
        for temp, p in enumerate(self.sgpa.parameters()):
            if temp < 3 + 2*(Num_layer-4):
                p.requires_grad = False
        self.optimizer = torch.optim.SGD(filter(lambda p: p.requires_grad, self.sgpa.parameters()), lr = 0.001*800, \
                                         momentum=0.5, weight_decay = 0.001/800)
        return self.sgpa, self.optimizer