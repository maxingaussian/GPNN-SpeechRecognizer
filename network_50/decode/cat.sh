for i in 50
do
    mkdir network_${i}
    for j in $(seq 5 1.0 10); do
	    for file_name in `cat /scratch/bdda/skhu/RM_decoding/lib/flists/file.lst` 
	    do
	        cat test_wp_${file_name:4:5}/network_${i}_LM_train_tg_scale_${j}/hd.train+test.tg.mlf >> network_${i}/network_${i}_scale_${j}_all.mlf
	    done
	    echo "HResults -A -z ::: -I /scratch/bdda/skhu/GPNN/dnn_baseline/DNN/test.mlf -n -e ::: \!SENT_START -e ::: \!SENT_END \
	    /scratch/bdda/xyliu/RM/import/releaseK/exp/R9/ml/treeg.list network_${i}/network_${i}_scale_${j}_all.mlf" > network_${i}/network_${i}_scale_${j}_all.sh      
	    chmod +x network_${i}/network_${i}_scale_${j}_all.sh
	    bash network_${i}/network_${i}_scale_${j}_all.sh >> network_${i}/network_${i}_scale_${j}_all_LOG
        rm -rf network_${i}/network_${i}_scale_${j}_all.sh
	done
done
