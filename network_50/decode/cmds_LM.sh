LM="/scratch/bdda/skhu/RM_decoding/lib/lm/tg_train.lm"
DCT="/scratch/bdda/skhu/GPNN/dnn_baseline/DNN/mono.hd.dct"
hmmList="/scratch/bdda/xyliu/RM/import/releaseK/exp/R9/ml/treeg.list"
config="/scratch/bdda/skhu/RM_decoding/lib/cfgs/config.hd"
PruneBeamWidth1=210.0
PruneBeamWidth2=210.0
MaxModelPruning=15000
n_size=24
LMScale=5
WordInsertionPenalty=0.0
for i in 50; do
    for file_name in `cat /scratch/bdda/skhu/RM_decoding/lib/flists/file.lst`
    do
        for j in $(seq 5 1.0 10); do
            {
                mkdir -p test_wp_${file_name:4:5}/network_${i}_LM_train_tg_scale_${j}
                echo "/scratch/bdda/xyliu/RM/import/home/htk3.5/exp-ar527/alpha3/bin.cpu/HDecode -A -D -V -T 1 -t 210.0 210.0 -u 15000 -s ${j} -p 0.0 -n 24 -C /scratch/bdda/skhu/RM_decoding/lib/cfgs/config_new.hd \
                -i test_wp_${file_name:4:5}/network_${i}_LM_train_tg_scale_${j}/hd.train+test.tg.mlf -H /scratch/bdda/skhu/GPNN/dnn_baseline/make_MMF/MMF \
                -d hmm0 -w $LM -X lat -S ../flists/${file_name:4:5}.scp /scratch/bdda/skhu/RM_decoding/lib/dicts/mono.hd.dct \
                /home/xyliu/RM/import/releaseK/lib/mlists/treeg.list" > test_wp_${file_name:4:5}/network_${i}_LM_train_tg_scale_${j}/run_file_${file_name:4:5}.sh        
                chmod +x test_wp_${file_name:4:5}/network_${i}_LM_train_tg_scale_${j}/run_file_${file_name:4:5}.sh
                bash test_wp_${file_name:4:5}/network_${i}_LM_train_tg_scale_${j}/run_file_${file_name:4:5}.sh > test_wp_${file_name:4:5}/network_${i}_LM_train_tg_scale_${j}/LOG
                rm -rf test_wp_${file_name:4:5}/network_${i}_LM_train_tg_scale_${j}/run_file_${file_name:4:5}.sh

            } &
        done
    done
done