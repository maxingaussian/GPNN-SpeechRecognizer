for file_name in `cat /scratch/bdda/skhu/RM_decoding/lib/flists/file.lst`
do
    {
        echo "copy-feats-to-htk --output-dir=htk_posdata --output-ext=post --sample-period=100000 ark:ark_data/ind_${file_name:4:5}.output" > ind_${file_name:4:5}.sh
        bash ind_${file_name:4:5}.sh
        rm -rf ind_${file_name:4:5}.sh
    } &
done