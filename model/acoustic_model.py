import time
import pickle
from os import walk
import numpy as np
import torch
import torch.nn as nn
import torch.utils.data as Data
import torch.nn.functional as F
from .gpnn import GPNN
from .speech_dataloader import thread_loaddata
from .speech_testdataloader import thread_testloaddata
from .learning_rate_schudule import learning_rate_schudule

class AcousticModel(object):
    
    def __init__(self, layer_sizes, trian_path, valid_path, train_num, valid_num,
                 batch_size=800, gpu_id=-1, kernel='gsk', tied='col',
                 concat_input=False, test_baseline=False):
        self.layer_sizes = layer_sizes
        self.train_loader = thread_loaddata(trian_path, 400)
        self.valid_loader = thread_loaddata(valid_path, 4)
        self.trian_path = trian_path
        self.valid_path = valid_path
        self.train_num = train_num
        self.valid_num = valid_num
        self.batch_size = batch_size
        self.kernel, self.tied = kernel, tied
        self.concat_input = concat_input
        self.test_baseline = test_baseline
        self.loss_func = nn.CrossEntropyLoss()
        if torch.cuda.is_available() and gpu_id >= 0:
            torch.cuda.set_device(gpu_id)
            self.device = torch.device('cuda')
        else:
            self.device = torch.device('cpu')
    
    def pretrain(self, epochs):
        for epoch in range(epochs):
            pretrain_layer_sizes = []
            pretrain_layer_sizes.append(self.layer_sizes[0])
            for i in range(epoch+1):
                pretrain_layer_sizes.append(self.layer_sizes[i+1])
            pretrain_layer_sizes.append(self.layer_sizes[-1])
            if epoch == 0 :
                self.GPNN = GPNN(pretrain_layer_sizes, sample_size=10,
                                 kernel=self.kernel, tied=self.tied,
                                 concat_input=self.concat_input,
                                 test_baseline=self.test_baseline)
                self.optimizer = torch.optim.Adam(self.GPNN.parameters(), lr=1e-3*2,
                                                  weight_decay=1e-3/self.batch_size, amsgrad=True)
            else:
                self.GPNN, self.optimizer = self.reload_network(pretrain_layer_sizes, dict_trained)
            self.GPNN.to(self.device)
            self.GPNN.train()
            running_time = -time.time()
            train_loss = 0
            train_ll_loss = 0
            train_kl_loss = 0
            train_accuracy = 0
            for iter, (batch_x, batch_y) in enumerate(self.train_loader):
                total_batch_num = (batch_y.size(0)-1)//self.batch_size+1
                for batch_num in range(total_batch_num):
                    b_x = batch_x[batch_num*self.batch_size:(batch_num+1)*self.batch_size,:].to(self.device)
                    b_y = batch_y[batch_num*self.batch_size:(batch_num+1)*self.batch_size].to(self.device)
                    train_output = self.GPNN(b_x)
                    train_KL = self.GPNN.kl_cost()
                    loss, nll, kl = self.cal_loss(b_y, train_output, train_KL)
                    self.optimizer.zero_grad()
                    loss.backward()
                    nn.utils.clip_grad_norm_(self.GPNN.parameters(), 0.32)
                    self.optimizer.step()
                    train_loss += loss.item()
                    train_ll_loss += nll.item()
                    train_kl_loss += kl.item()
                    train_y_decoded = torch.max(train_output, 1)[1].data.squeeze()
                    train_accuracy += torch.sum(torch.eq(train_y_decoded, b_y)).item()/self.train_num                    
            running_time += time.time()
            print('Epoch: ', epoch, '| time: %.2fs' % running_time, '| train accuracy: %.4f' \
                  % train_accuracy, '| train_ll_loss: %.2f' % train_ll_loss, '| train_kl_loss: %.2f' % train_kl_loss)
            self.GPNN.eval()
            valid_accuracy, valid_loss = self.valid()
            print('Epoch: ', epoch, '| valid loss: %.2f' % valid_loss, '| valid accuracy: %.2f' % valid_accuracy)
            dict_trained = self.GPNN.state_dict()
        for temp, p in enumerate(self.GPNN.parameters()):
            p.requires_grad = True
        self.optimizer = torch.optim.Adam(self.GPNN.parameters(), lr=1e-3*3,
                                          weight_decay=1e-3/self.batch_size, amsgrad=True)
    
    def reload_network(self, layer_sizes, dict_trained):
        num_layer = len(layer_sizes)
        self.GPNN = GPNN(layer_sizes, sample_size=10,
                         kernel=self.kernel, tied=self.tied,
                         concat_input=self.concat_input,
                         test_baseline=self.test_baseline)
        dict_new = self.GPNN.state_dict()
        trained_list = list(dict_trained.keys())
        new_list = list(dict_new.keys())
        num_vgpa_params = 0
        for i, p in enumerate(self.GPNN.vgpa_layer.parameters()):
            num_vgpa_params += 1
            dict_new[new_list[i]] = dict_trained[trained_list[i]]
        for i in range(2*(num_layer-4)):
            dict_new[new_list[i+num_vgpa_params]] = dict_trained[trained_list[i+num_vgpa_params]]
        self.GPNN.load_state_dict(dict_new)
        for i, p in enumerate(self.GPNN.parameters()):
            if(i < num_vgpa_params+2*(num_layer-4)):
                p.requires_grad = False
        self.optimizer = torch.optim.Adam(filter(lambda p: p.requires_grad, self.GPNN.parameters()),
                                          lr=1e-3*2, weight_decay=1e-3/self.batch_size, amsgrad=True)
        return self.GPNN, self.optimizer
    
    def train(self, epochs, pretrain=True, max_iter=5):
        if(pretrain):
            self.pretrain(len(self.layer_sizes)-2)
        else:
            self.GPNN = GPNN(self.layer_sizes, sample_size=10,
                             kernel=self.kernel, tied=self.tied,
                             test_baseline=self.test_baseline)
            self.GPNN.to(self.device)
            self.optimizer = torch.optim.Adam(self.GPNN.parameters(), lr=1e-3,
                                              weight_decay=1e-3/self.batch_size, amsgrad=True)
        best_state, count, best_valid_accuracy, valid_accuracy_history = {}, 0, 0, []
        for epoch in range(epochs):
            self.GPNN.train()
            running_time = -time.time()
            train_accuracy, train_loss, train_ll_loss, train_kl_loss = 0, 0, 0, 0
            for iter, (batch_x, batch_y) in enumerate(self.train_loader):
                total_batch_num = (batch_y.size(0)-1)//self.batch_size+1
                for batch_num in range(total_batch_num):
                    b_x = batch_x[batch_num*self.batch_size:(batch_num+1)*self.batch_size,:].to(self.device)
                    b_y = batch_y[batch_num*self.batch_size:(batch_num+1)*self.batch_size].to(self.device)
                    train_output = self.GPNN(b_x)
                    train_KL = self.GPNN.kl_cost()
                    loss, nll, kl = self.cal_loss(b_y, train_output, train_KL)
                    self.optimizer.zero_grad()
                    loss.backward()
                    nn.utils.clip_grad_norm_(self.GPNN.parameters(), 0.32)
                    self.optimizer.step()
                    train_loss += loss.item()
                    train_ll_loss += nll.item()
                    train_kl_loss += kl.item()
                    train_y_decoded = torch.max(train_output, 1)[1]#data.squeeze()
                    train_accuracy += torch.sum(torch.eq(train_y_decoded, b_y)).item()/self.train_num
            learning_rate = self.optimizer.param_groups[0]['lr']
            running_time += time.time()
            print('Epoch: ', epoch, '| time: %.2fs' % running_time, '| learning rate: %.2e' % learning_rate,
                  '| train accuracy: %.4f' % train_accuracy, '| train_ll_loss: %.2f' % train_ll_loss, \
                  '| train_kl_loss: %.2f' % train_kl_loss)
            self.GPNN.eval()
            valid_accuracy, valid_loss = self.valid()
            valid_accuracy_history.append(valid_accuracy)
            if(best_valid_accuracy < valid_accuracy):
                best_valid_loss = valid_loss
                best_valid_accuracy = valid_accuracy
                count = 0
                self.GPNN.cpu()
                best_state = self.GPNN.state_dict()
                torch.save(best_state, 'model_parameters/best_net.pkl')
                self.GPNN.to(self.device)
            else: 
                count += 1
            self.optimizer, self.GPNN = learning_rate_schudule(
                self.GPNN, self.optimizer, valid_accuracy, best_valid_accuracy,
                best_state, startdiff=1e-3, mode='max', factor=0.5, patience=2)
            print('Epoch: ', epoch, '| best valid loss: %.4f' % best_valid_loss,
                  '| best valid accuracy: %.4f' % best_valid_accuracy,
                  '| valid loss: %.4f' % valid_loss, '| valid accuracy: %.4f' % valid_accuracy)
            if count > max_iter:
                model_param_files = []
                for (dirpath, dirnames, filenames) in walk('model_parameters'):
                    model_param_files.extend(filenames)
                    break
                model_param_files.remove('best_net.pkl')
                get_acc = lambda s: float(s[:-4].split('_')[-1])
                accs = list(map(get_acc, model_param_files))
                max_acc = max(accs)
                if(valid_accuracy > max_acc):
                    torch.save(best_state, 'model_parameters/net_%.1f_%.4f.pkl'%(best_valid_loss, best_valid_accuracy))
                print('No improvement for %d epochs' % max_iter)
                break
        return valid_accuracy_history
    
    def valid(self):
        valid_loss = 0
        valid_accuracy = 0
        for step, (batch_valid_x, batch_valid_y) in enumerate(self.valid_loader):
            b_valid_x = batch_valid_x.to(self.device)
            b_valid_y = batch_valid_y.to(self.device)
            valid_output = self.GPNN(b_valid_x).to(self.device)
            valid_KL = self.GPNN.kl_cost()
            loss, nll, kl = self.cal_loss(b_valid_y, valid_output, valid_KL)
            valid_loss += nll.item()
            valid_y_decoded = torch.max(valid_output, 1)[1]
            valid_accuracy += torch.sum(torch.eq(valid_y_decoded,b_valid_y)).item()/self.valid_num
        return valid_accuracy, valid_loss
    
    def test(self, parameter_file=None):
        with open('../prior.pickle','rb') as prior_file:
            prior = pickle.load(prior_file)
        if(parameter_file is not None):
            self.GPNN = GPNN(self.layer_sizes, sample_size=20,
                             kernel=self.kernel, tied=self.tied,
                             concat_input=self.concat_input,
                             test_baseline=self.test_baseline)
            self.GPNN.to(self.device)
            self.GPNN.load_state_dict(torch.load(parameter_file))
        softmax = nn.Softmax(dim=1)
        for scp_name in ['ind_feb89', 'ind_feb91', 'ind_oct89', 'ind_sep92']:
            test_path = '/scratch/bdda/skhu/RM/lib/flists/'+scp_name+'.scp'
            test_loader = thread_testloaddata(test_path, 1)
            test_path_list = []
            with open(test_path, 'r') as f3:
                for line in f3.readlines():
                    line = line.strip()
                    test_path_list.append(line)              
            write_path = 'ark_data/'+scp_name+'.output'
            f = open(write_path, 'w')
            self.GPNN = self.GPNN.to(self.device)
            self.GPNN.eval()
            for iter, (batch_x, batch_y) in enumerate(test_loader):
                file_name = test_path_list[iter].split('/')[-1]
                X_test = torch.FloatTensor(batch_x).to(self.device)
                test_output = self.GPNN(X_test)
                test_output = softmax(test_output)
                output = np.array(test_output.detach().cpu().numpy()) + (1e-40)*np.ones(689)
                output = np.log(output)-np.log(prior)
                f.write(file_name.split('.')[0]+'  ['+'\n')
                R = output.shape[0]
                for i in range(R-1):
                    f.write('  ')
                    f.write(' '.join(str(v) for v in output[i,:].tolist()))
                    f.write(' \n')
                f.write('  ')
                f.write(' '.join(str(v) for v in output[R-1,:].tolist())+' ]')
                f.write('\n')
            f3.close()
            f.close()
    
    def cal_loss(self, y, output, KL):
        nll = self.loss_func(output, y)
        kl = KL/output.size(0)
        loss = nll + kl
        return loss, nll, kl