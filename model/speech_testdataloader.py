import torch
import torch.utils.data as data
import pandas as pd
import numpy as np
import pickle
import os

class Speech_testDatasetFolder(data.Dataset):     
    """A generic data loader where
        the samples are arranged in this way: ::
        root/xxx.csv 
        root/xxy.csv
        root/xxz.csv     
    Args: input_path (string): directory path.
    Attributes:         input_list (list): List of (sample path) tuples
    """
    def __init__(self, input_path):
        with open(input_path, 'r') as f:
            lines = f.readlines()
            self.input_list = [i.strip() for i in lines]

    def __getitem__(self, index):
        """
        Args:
            index (int): Index
        Returns:
            tuple: (input_concat, label_data) where label_data is class_index of the target class.
        """
        input_path = self.input_list[index]
        # label_path = input_path.replace('inputdata_txt','outputdata_label').replace('mfc','csv')

        input_data = pd.read_table(input_path,'\s+',index_col=None,header=None)
        # label_data = pd.read_csv(label_path, sep=" ", header=None,  index_col=None)
        input_data = np.array(input_data)
        label_data = np.ones((input_data.shape[0],1))

        row_num = input_data.shape[0]
        column_num = input_data.shape[1]
        X_input = np.zeros((row_num, 9*column_num))
        for i in range(row_num):
            if i == 0:
                X_input[i,:] = np.hstack((input_data[i,:],input_data[i,:],input_data[i,:],input_data[i,:],input_data[i,:],input_data[i+1,:],\
            input_data[i+2,:],input_data[i+3,:],input_data[i+4,:]))
            elif i == 1:
                X_input[i,:] = np.hstack((input_data[i-1,:],input_data[i-1,:],input_data[i-1,:],input_data[i-1,:],input_data[i,:],input_data[i+1,:],\
            input_data[i+2,:],input_data[i+3,:],input_data[i+4,:]))
            elif i == 2:
                X_input[i,:] = np.hstack((input_data[i-2,:],input_data[i-2,:],input_data[i-2,:],input_data[i-1,:],input_data[i,:],input_data[i+1,:],\
            input_data[i+2,:],input_data[i+3,:],input_data[i+4,:]))
            elif i == 3:
                X_input[i,:] = np.hstack((input_data[i-3,:],input_data[i-3,:],input_data[i-2,:],input_data[i-1,:],input_data[i,:],input_data[i+1,:],\
            input_data[i+2,:],input_data[i+3,:],input_data[i+4,:]))
            elif i == row_num-4:
                X_input[i,:] = np.hstack((input_data[i-3,:],input_data[i-3,:],input_data[i-2,:],input_data[i-1,:],input_data[i,:],input_data[i+1,:],\
            input_data[i+2,:],input_data[i+3,:],input_data[i+3,:]))
            elif i == row_num-3:
                X_input[i,:] = np.hstack((input_data[i-3,:],input_data[i-3,:],input_data[i-2,:],input_data[i-1,:],input_data[i,:],input_data[i+1,:],\
            input_data[i+2,:],input_data[i+2,:],input_data[i+2,:]))
            elif i == row_num-2:
                X_input[i,:] = np.hstack((input_data[i-3,:],input_data[i-3,:],input_data[i-2,:],input_data[i-1,:],input_data[i,:],input_data[i+1,:],\
            input_data[i+1,:],input_data[i+1,:],input_data[i+1,:]))
            elif i == row_num-1:
                X_input[i,:] = np.hstack((input_data[i-3,:],input_data[i-3,:],input_data[i-2,:],input_data[i-1,:],input_data[i,:],input_data[i,:],\
            input_data[i,:],input_data[i,:],input_data[i,:]))
            else:
                X_input[i,:] = np.hstack((input_data[i-4,:],input_data[i-3,:],input_data[i-2,:],input_data[i-1,:],input_data[i,:],input_data[i+1,:],\
            input_data[i+2,:],input_data[i+3,:],input_data[i+4,:]))
        return X_input, label_data

    def __len__(self):
        return len(self.input_list)


def thread_testloaddata(target_path, file_num):
    target_path = os.path.abspath(target_path)
    dataset = Speech_testDatasetFolder(target_path) 
    loader = data.DataLoader(
        dataset = dataset,
        batch_size = file_num,
        shuffle = False,
        num_workers = 5,
        collate_fn = test_collate_data,
    )
    return loader

def test_collate_data(batch):
    input_data, label_data = zip(*batch)
    input_list = []
    label_list = []
    for i in range(len(label_data)):
        input_list.append(input_data[i])
        label_list.append(label_data[i])
    input_numpy = np.concatenate(input_list, axis=0)
    label_numpy = np.concatenate(label_list, axis=0)
    concat_numpy = np.hstack((input_numpy, label_numpy))
    # np.random.shuffle(concat_numpy)
    input_tensor_shuffle = torch.FloatTensor(concat_numpy[:,0:-1])
    label_tensor_shuffle = torch.LongTensor(concat_numpy[:,-1])
    return input_tensor_shuffle, label_tensor_shuffle
