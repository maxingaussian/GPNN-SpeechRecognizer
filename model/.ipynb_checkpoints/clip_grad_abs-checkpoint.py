import torch
import numpy as np

def clip_grad_abs(parameters, max_norm):
    parameters = list(filter(lambda p: p.grad is not None, parameters))
    max_norm = float(max_norm)
    for p in parameters:
        p.grad.data[p.grad.data>max_norm] = max_norm
        p.grad.data[p.grad.data<-max_norm] = -max_norm