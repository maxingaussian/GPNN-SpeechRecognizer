import torch
import torch.nn as nn
import torch.utils.data as Data
from torch.autograd import Variable
import matplotlib.pyplot as plt
import torch.nn.functional as F
import numpy as np

class VGPALayer(nn.Module):
    
    """ Variational Gaussian Process Activation """

    def __init__(self, input_dim, output_dim, kernel='gsk', tied='col'):
        assert output_dim%2==0, "output dimension must be an even number"
        super(VGPALayer, self).__init__()
        self.input_dim              = input_dim
        self.output_dim             = output_dim
        self.kernel                 = kernel
        self.tied                   = tied
        self.frequency_mean         = nn.Parameter(torch.empty(input_dim, output_dim))
        if(tied == 'row'):
            self.frequency_lgstd    = nn.Parameter(torch.empty(output_dim))
        elif(tied == 'col'):
            self.frequency_lgstd    = nn.Parameter(torch.empty(input_dim, 1))
        else:
            self.frequency_lgstd    = nn.Parameter(torch.empty(input_dim, output_dim))

    def reset_parameters(self):
        nn.init.xavier_normal_(self.frequency_mean)
        nn.init.uniform_(self.frequency_lgstd)
    
    def kl_divergence(self):
        mean_square = self.frequency_mean**2
        std_square = torch.exp(self.frequency_lgstd*2)
        lgvar = 2*self.frequency_lgstd
        return .5*torch.sum(mean_square+std_square-lgvar-1)

    def forward(self, input):
        device = next(self.parameters()).device
        frequency_std = torch.exp(self.frequency_lgstd)
        if(self.tied == 'row'):
            epsilon = torch.randn(self.output_dim).to(device)
        elif(self.tied == 'col'):
            epsilon = torch.randn(self.input_dim, 1).to(device)
        else:
            epsilon = torch.randn(self.input_dim, self.output_dim).to(device)
        if(self.kernel == 'gsk'):
            frequency = self.frequency_mean+epsilon*frequency_std
            frequency_left, frequency_right = input.mm(frequency).chunk(2, -1)
            output = torch.cat([frequency_left.cos()+frequency_right.cos(),
                                frequency_left.sin()+frequency_right.sin()], -1)
        elif(self.kernel == 'ss'):
            frequency = self.frequency_mean+epsilon*frequency_std
            frequency_left, frequency_right = input.mm(frequency).chunk(2, -1)
            output = torch.cat([frequency_left.cos(), frequency_right.sin()], -1)
        return output

    def __repr__(self):
        return self.__class__.__name__ + '(' \
            + 'input_dim=' + str(self.input_dim) \
            + ', output_dim=' + str(self.output_dim) + ')'



class GPNN(nn.Module):
    
    def __init__(self, layer_sizes, sample_size=10,
                 activation='sigmoid', kernel='gsk', tied='col',
                 concat_input=False, test_baseline=False):
        super(GPNN, self).__init__()
        self.layer_sizes = layer_sizes
        self.sample_size = sample_size
        self.activation = activation
        self.concat_input = concat_input
        self.test_baseline = test_baseline
        if(self.test_baseline):
            self.vgpa_layer = nn.Linear(layer_sizes[0], layer_sizes[1])
        else:
            self.vgpa_layer = VGPALayer(layer_sizes[0], layer_sizes[1], kernel=kernel, tied=tied)
        self.dnn_layers = self.multilayer_perceptron(layer_sizes)
        self.vgpa_layer.reset_parameters()

    def multilayer_perceptron(self, layer_sizes):
        dnn_layers = nn.Sequential()
        for l, (n_in, n_out) in enumerate(zip(layer_sizes[1:-1], layer_sizes[2:])):
            if(self.concat_input):
                dnn_layers.add_module('linear'+str(l+1), nn.Linear(n_in+self.layer_sizes[0], n_out))
            else:
                dnn_layers.add_module('linear'+str(l+1), nn.Linear(n_in, n_out))
            if(l < len(layer_sizes)-3):
                if(self.activation.lower() == 'sigmoid'):
                    activation_layer = nn.Sigmoid()
                elif(self.activation.lower() == 'tanh'):
                    activation_layer = nn.Tanh()
                elif(self.activation.lower() == 'relu'):
                    activation_layer = nn.ReLU()
                dnn_layers.add_module(self.activation+str(l), activation_layer)
        return dnn_layers

    def kl_cost(self):
        if(self.test_baseline):
            KL = torch.norm(self.vgpa_layer.weight)
        else:
            KL = self.vgpa_layer.kl_divergence()
        return KL

    def single_pass(self, input):
        output = self.vgpa_layer(input)
        for l in range(len(self.dnn_layers)):
            if(self.concat_input and l%2 == 0):
                output = self.dnn_layers[l](torch.cat([input, output], -1))
            else:
                output = self.dnn_layers[l](output)
        return output
    
    def forward(self, input):
        if(self.training or self.test_baseline):
            return self.single_pass(input)
        else:
            outputs = []
            for l in range(self.sample_size):
                outputs += [self.single_pass(input).detach().cpu().numpy()]
        return torch.Tensor(np.median(outputs, 0))
    