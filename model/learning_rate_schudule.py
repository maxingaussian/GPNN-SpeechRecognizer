def learning_rate_schudule(model, optimizer, monitor_value, local_monitor_value, best_state,
                           startdiff=0.001, mode='max', factor=0.5, patience=1):
    global patience_count
    if mode == 'max':
        if monitor_value - local_monitor_value < startdiff and monitor_value - local_monitor_value > 0:
            optimizer.param_groups[0]['lr'] = optimizer.param_groups[0]['lr']*factor
        if monitor_value < local_monitor_value:
            patience_count += 1
            if patience_count >= patience:
                model.load_state_dict(best_state)
                optimizer.param_groups[0]['lr'] = optimizer.param_groups[0]['lr']*factor
                patience_count = 0
        else: patience_count = 0
    else:
        if local_monitor_value - monitor_value < startdiff and local_monitor_value - monitor_value > 0:
            optimizer.param_groups[0]['lr'] = optimizer.param_groups[0]['lr']*factor 
        if monitor_value > local_monitor_value:
            patience_count += 1
            if patience_count >= patience:
                model.load_state_dict(best_state)
                optimizer.param_groups[0]['lr'] = optimizer.param_groups[0]['lr']*factor
                patience_count = 0
        else: patience_count = 0
    return optimizer, model